# Turning a docker image into a running container

In the previous lab we finished by pulling down our docker image running in our local registry.

We know this image is built on top of the `nginx:alpine` image found [here](https://hub.docker.com/_/nginx/).


## Creating the container

We can now turn this image into a running container by executing the following command:

```
docker run --name my-container -d -p 9999:80 localhost:5000/my-container:1.2.3
```

Some comments about the command above:

1. The `-d` flag will run the container in a detached mode.

2. The `-p` flag binds port 80 of the container to port 9999 on the host.


To see the running container execute the following command:

```
docker ps -a
```

You should see the following output:

```
CONTAINER ID        IMAGE                COMMAND                  CREATED             STATUS              PORTS                  NAMES
45a1819b7e0b        localhost:5000/my-container:1.2.3   "nginx -g 'daemon ..."   2 seconds ago       Up 1 second         0.0.0.0:9999->80/tcp   my-container
```

## Validation

Now in your browser browse to [http://localhost:9999](http://localhost:9999).

You should see the following:

```
Docker 101

Welcome to the docker 101 workshop!
Created by: Steven Wade

Contact information: swade@apprenda.com.

Version: 1.0
```