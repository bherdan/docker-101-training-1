# Building your own docker image

Running another persons image is cool but creating your own is even cooler.

This document will detail the steps to create your own container, these are:

1. Creating a Dockerfile
2. Using `docker build` to build the image via the Dockerfile

## Prerequisites
Clone the repository
```
git clone https://dkassab@bitbucket.org/dkassab/docker-101-training.git
```
For this session you will need to browse to the `demos\build` directory (see below).

```
cd docker-101-training/demos/build
ls -lart
```

In this directory we can see four files (see below)

```
drwxr-xr-x  3 StevenWade  staff  102 Sept 28 16:21 ..
-rw-r--r--  1 StevenWade  staff   54 Sept 28 16:22 ignoreme.txt
-rw-r--r--  1 StevenWade  staff   12 Sept 28 16:22 .dockerignore
-rw-r--r--  1 StevenWade  staff   89 Sept 28 16:26 Dockerfile
-rw-r--r--  1 StevenWade  staff  332 Sept 28 16:27 index.html
drwxr-xr-x  6 StevenWade  staff  204 Sept 28 16:27 .
```

## File Breakdown

### ignoreme.txt

This is a file which we don't want added to our image as its not required.

### .dockerignore

This is a file which contains a list of files and directories we don't want added to our image.

### Dockerfile

This is a file which is used when building the docker image.

The contents of this file can be seen below:

```
FROM nginx:alpine
MAINTAINER Steve Wade <swade@apprenda.com>
COPY . /usr/share/nginx/html
```

- The `FROM` is the image we are going to base our image from.
- The `MAINTAINER` is a piece of metadata containing who the maintainer of the image is.
- The `COPY` command is copying the contents of the current directory to a given directory.

Note: The `COPY` command above references the `.dockerignore` and doesn't copy the files or directories listed in that file.

### index.html

This file contains the website content which be rendered in the browser.

## Building the image

To build the docker image simply execute the following command:

```
docker build --no-cache=true .
```

Note: We are using the `--no-cache=true` flag so that all image layers are built and the image cache is not utilised.

You should see an output similar to the following:

```
Sending build context to Docker daemon  4.096kB
Step 1/3 : FROM nginx:alpine
 ---> bf85f2b6bf52
Step 2/3 : MAINTAINER Steve Wade <swade@apprenda.com>
 ---> Using cache
 ---> b1a902ac30d7
Step 3/3 : COPY . /usr/share/nginx/html
 ---> Using cache
 ---> 8d5047ead34b
Successfully built 8d5047ead34b
```

To verify the build was a success execute the following command:

```
docker images
```

You should see an output similar to the following:

```
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
<none>              <none>              a3c7faef9e0c        4 seconds ago       15.5MB
```