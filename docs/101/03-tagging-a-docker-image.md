# Tagging a docker image

When building a docker image using simply `docker build` the name of the image created is random.

In this lab we will walk through how to tag an image with a specific name.

## Prerequisites

For this session you will need to browse to the `demos\build` directory (see below).

```
cd demos/build
```

## Without tags

To build the container image without tags execute the following command:

```
docker build .
```

Now execute the following command to see the docker image:

```
docker images
```

You should now see something similar to the below:

```
$  docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
<none>              <none>              a3c7faef9e0c        4 seconds ago       15.5MB
```

As you can see the `REPOSITORY` and `TAG` columns above are empty, this means the image does not have a name.

Therefore the only way to reference the image would be using the image id (a3c7faef9e0c).

## With tags (no version specified)

To build the container image with tags execute the following command:

```
docker build --no-cache=true -t my-container .
```

Now execute the following command to see the docker image:

```
docker images
```

You should now see something similar to the below:

```
$  docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
my-container        latest              a3c7faef9e0c        14 minutes ago      15.5MB
```

Note: Because we never specified an image tag, the tag of `latest` was chosen.

## With tags (version specified)

To build the container image with tags and a version execute the following command:

```
docker build --no-cache=true -t my-container:1.2.3 .
```

Now execute the following command to see the docker image:

```
docker images
```

You should now see something similar to the below:

```
$  docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
my-container        1.2.3               a3c7faef9e0c        30 minutes ago      15.5MB
```

As you can see from above our my-container image has been given a version of 1.2.3 

We can also build the same image with a new version by switching `1.2.3` for something else.