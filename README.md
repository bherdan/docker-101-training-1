# Docker 101 Training Lab

## Prerequisites 

The prerequisites for this lab can be found [here](docs/00-prerequisites.md).

## Lab Breakdown

Please see below for a more detailed breakdown for this training lab.

Note: Clicking the link will take you to a step-by-step guide for that part of the lab.

1. [Building your own docker image using a Dockerfile.](docs/101/02-building-your-own-docker-image.md) 
2. [Tagging a docker image](docs/101/03-tagging-a-docker-image.md)
3. [Uploading an image to a docker registry.](docs/101/04-uploading-your-image-to-a-docker-registry.md)
4. [Running an existing docker image from a docker registry (e.g. Docker Hub)](docs/101/01-running-an-existing-docker-image.md)
5. [Turning a docker image into a running container.](docs/101/05-turning-a-docker-image-into-a-running-container.md)
6. [Interacting with a running container.](docs/101/06-interacting-with-a-running-container.md)
7. [Inspecting a docker image and running container.](docs/101/07-inspecting-a-docker-image-and-running-container.md)
8. [Obtaining the logs from a running container](docs/101/08-obtaining-the-logs-from-a-running-container.md)
9. [Removing running containers and existing docker images.](docs/101/09-removing-running-containers-and-existing-docker-images.md)